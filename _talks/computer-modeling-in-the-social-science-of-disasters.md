---
abstract: Do you know what everyone would do if Godzilla suddenly showed up in the
  room? Give social scientists enough computer power, and they can tell you. It wouldn't
  look anything like a disaster movie, and the gap between Hollywood guesses and what
  people can prove is where things get exciting.
level: All
speakers:
- Eileen Young
title: Computer Modeling in the Social Science of Disasters
type: talk
---

Computer modeling of exactly what happens during a disaster isn't just a cool way to understand things and make better disaster movies - it saves lives. And getting a social scientist involved means that it will be more accurate, because panic and perfectly rational behavior are both myths. Learn about disaster myths, exactly how social science can be used in disaster modeling, how it's being used now, and why having more software developers involved would be great for everyone.

After this talk, you should have some ideas about the ways programming and social science are working together to make the future both more exciting and more predictable - and maybe ideas for a project of your own. There will be a somewhat long question period because of the subject matter.