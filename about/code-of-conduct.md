---
title: Code of Conduct
---

PyGotham is dedicated to providing a harassment-free conference experience for
everyone. We do not tolerate harassment of conference participants in any form.
Conference participants violating these rules may be sanctioned or expelled from
the conference without a refund at the discretion of the conference organizers.

Harassment includes offensive verbal comments related to gender, gender identity
and expression, sexual orientation, disability, physical appearance, body size,
race, religion, sexual images in public spaces, deliberate intimidation,
stalking, following, harassing photography or recording, sustained disruption of
talks or other events, inappropriate physical contact, and unwelcome sexual
attention. Participants asked to stop any harassing behavior are expected to
comply immediately.

Exhibitors in the expo hall, sponsor or vendor booths, or similar activities are
also subject to the anti-harassment policy. In particular, exhibitors should not
use sexualized images, activities, or other material. Booth staff (including
volunteers) should not use sexualized clothing/uniforms/costumes, or otherwise
create a sexualized environment.

If a participant engages in harassing behavior, the conference organizers may
take any action they deem appropriate, including warning the offender or
expulsion from the conference with no refund.  In the event of serious
infractions of the code of conduct, {{ site.data.event.name }} staff may share
incident details and offenders' names with third parties including but not
limited to: venue staff, local law enforcement, and staff of other conferences.
If you are being harassed, notice that someone else is being harassed, or have
any other concerns, please contact a member of conference staff immediately.
Conference staff can be identified by t-shirts/special badges.


PyGotham has an additional rule of no videotaping or photography without prior
permission. If you wish to photograph, film or record any attendee, including
speakers, you must first ask for their permission to do so.

Conference staff will be happy to help participants contact hotel/venue security
or local law enforcement, provide escorts, or otherwise assist those
experiencing harassment to feel safe for the duration of the conference. We
value your attendance.

Email address for organizers
: [organizers@pygotham.org](mailto:organizers@pygotham.org)

Phone number for conference organizers
: 929-322-3476

Local law enforcement, emergency
: 911

Local law enforcement, non-emergency
: 311

Local sexual assault hotline
: 800-621-HOPE (4673) or dial 311

Local medical, emergency
: 911

Local medical, non-emergency
: 311

Local taxi company
: 800-609-8731

We expect participants to follow these rules at all conference venues and
conference-related social events.
