require 'date'
require 'liquid'

module Schedule
  def add_minutes(dt, delta)
    return dt + delta * 60
  end

  def slotted_schedule(talks, rooms)
    talks_by_day = talks.group_by { |t| t['slot'].to_date }

    talks_by_day.each do |date, talks|
      talks_by_day[date] = talks.group_by { |t| t['slot'] }

      talks_by_day[date].each do |key, value|
        if value.length == 1
          next
        end
        value.sort! { |a, b| rooms.index(a['room']) <=> rooms.index(b['room']) }
      end
    end

    return talks_by_day
  end
end

Liquid::Template.register_filter(Schedule)
