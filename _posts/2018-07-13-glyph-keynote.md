---
title: "Announcing our third keynote: Glyph"
date: 2018-07-13 10:00:00 -0400
image: /uploads/posts/glyph.jpg
excerpt_separator: <!--more-->
---

[Glyph](https://en.wikipedia.org/wiki/Glyph_Lefkowitz) is mostly a computer
programmer, mostly in [Python](https://www.python.org/).<!--more-->

Although most well-known for being the founder of [the Twisted
project](https://twistedmatrix.com/), Glyph has also worked on [massively
multiplayer online
games](https://en.wikipedia.org/wiki/Ultima_Worlds_Online:_Origin), [dynamic web
applications](https://github.com/twisted/quotient), [enterprise information
management software](https://www.calendarserver.org/), and created or
contributed to [dozens of open source projects](https://github.com/glyph),
mostly related to infrastructure.

He’s also a [prolific public speaker](http://pyvideo.org/speaker/glyph.html),
mostly about software, also occasionally about ethics.

He has run Python programs, and written Python programs to be run, on
mainframes, on custom-built embedded devices, and just about everything in
between.

Currently, he works at [Pilot](https://pilot.com/), making sure the numbers
always add up.

In closing, he hopes you’re having a nice day.
