---
name: Bloomberg
tier: silver
site_url: https://www.techatbloomberg.com/
logo: bloomberg.png
---
Bloomberg technology drives the world’s financial markets. More than 5,000 engineers define,
architect, build and deploy complete systems to fulfill the needs of leading financial market
participants globally. We trust our teams to choose the right technologies for the job, and, at
Bloomberg, the answer is often Python. We employ an active community of more than 2,000 Python
developers who have their hands in everything: from financial analytics to data science to
contributing to open source technologies like Project Jupyter. We also sponsor and host many events
for the Python community at large.
