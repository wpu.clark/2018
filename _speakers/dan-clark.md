---
name: Dan Clark
talks:
- Database Testing with pytest
---

My name is Dan Clark, and I wear too many hats. My career has never taken hold in one place, and I am always shifting my focus. 

I am the father of two amazing kids as well as the husband of a wonderful woman where we live in a small house in northwest New Jersey. I wake up as early as possible to tinker so that I can learn something new every day, and I try my best to share those things with the people around me. I am also a co-organizer of the Python User Group in Princeton, where I organize events to help others share and learn about Python.

Luckily, I was reluctantly introduced to Python over 7 years ago, but now I want it in my life as much as possible. It makes my life and career easier, and I want to share my knowledge in the hope that others will be able to take away something.