---
name: Mike Perez
talks:
- 'Talking About Burnout: An Attempt of Coping with Python and Music'
---

Mike Perez has been contributing to open source software for 19-years before he even knew what it was. Most recently he was a contributor of OpenStack for 8-years. He was part of the the OpenStack Foundation team as a developer coordinator/advocate focusing on community health, on-boarding, cross-project initiatives, a member of the OpenStack Technical Committee and core contributor of the core OpenStack Ansible module.  He was formerly the project technical lead (PTL) and core member of the OpenStack Block Storage service, Cinder, for the Kilo and Liberty release. He has also worked on the integration of Ceph RBD in OpenStack and vhost-scsi from the Linux kernel.

When he's fed up with computers he enjoys making coffee or homemade pizza for his friends and family, dancing, attempting karaoke, playing modular synthsizers and skateboarding.