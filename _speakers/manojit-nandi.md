---
name: Manojit Nandi
talks:
- 'Interpretable Machine Learning: Methods for understanding complex models'
---

I am a Senior Data Scientist at Rocketrip where I use data to help companies reduce spending on corporate travel. I have previously worked as a Data Scientist at STEALTHbits Technologies and Verizon Wireless. I was a fellow in the 2013 Data Science for Social Good fellowship where I became interested in the potential capabilities for using data science to solve problems with high societal impact.