---
name: Boudewijn Aasman
talks:
- Coding by Voice with Dragonfly
---

I'm the senior data science engineer at the Center for Health Data Innovations at Montefiore hospital in the Bronx. I started my career using R, but once I played around with Python to see what it was all about, I got hooked and never looked back. I come from a statistics background but moved towards the data science and data engineering fields soon after graduating.